let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];



let renderGlassesList = () => {
    let contentHTML = '';
    for(let i = 0; i < dataGlasses.length; i++){
        let glasses = `
            <img class="col-4" src="${dataGlasses[i].src}" id="${dataGlasses[i].id}" alt="" />
        `
        contentHTML += glasses;
    }
    document.getElementById("vglassesList").innerHTML = contentHTML;
}
renderGlassesList();

let glassesList = document.getElementsByTagName("img");
for(let index = 0; index < glassesList.length; index++){
    glassesList[index].addEventListener("click", function(){
        let id = glassesList[index].id;
        let glasses = getGlasses(id, dataGlasses);
        document.getElementById("avatar").innerHTML = `
        <img src="${glasses.virtualImg}" alt="" />
        `;
        document.getElementById("glassesInfo").innerHTML = `
        <h5>${glasses.name} - ${glasses.brand} (${glasses.color})</h5>
        <span class="bg-danger text-white px-2 py-1 rounded">&dollar;${glasses.price}</span>
        <span class="text-success ml-2">Stocking</span>
        <p class="pt-3 m-0">${glasses.description}</p>
        `
        document.getElementById("glassesInfo").style.display = "block";
    })
}

let getGlasses = (id, list) => {
    for(let i = 0; i< list.length; i++){
        if (list[i].id == id){
            return list[i];
        }
    }
    return -1;
}

function removeGlasses(input) {
    let element = document.getElementById("avatar");
    let img = element.querySelector("img");
    if (input == true){
        img.style.display= "block";
    } else {
        img.style.display= "none";
    }
}
window.removeGlasses = removeGlasses;